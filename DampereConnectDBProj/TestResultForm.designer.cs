﻿using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace DampereConnectDBProj
{
    partial class TestResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.dgvShowDB = new System.Windows.Forms.DataGridView();
            this.cmbTest = new System.Windows.Forms.ComboBox();
            this.myChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.myChart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pnlContain = new System.Windows.Forms.Panel();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblSelectDate = new System.Windows.Forms.Label();
            this.lblSelectTo = new System.Windows.Forms.Label();
            this.btnSearchByTime = new System.Windows.Forms.Button();
            this.lblSelectByNo = new System.Windows.Forms.Label();
            this.txtSelectInput = new System.Windows.Forms.TextBox();
            this.btnSearchByNo = new System.Windows.Forms.Button();
            this.btnSearchAll = new System.Windows.Forms.Button();
            this.btnClearDate = new System.Windows.Forms.Button();
            this.pnlbg = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myChart2)).BeginInit();
            this.pnlContain.SuspendLayout();
            this.pnlbg.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvShowDB
            // 
            this.dgvShowDB.AllowUserToAddRows = false;
            this.dgvShowDB.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShowDB.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvShowDB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShowDB.Location = new System.Drawing.Point(77, 186);
            this.dgvShowDB.Name = "dgvShowDB";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvShowDB.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvShowDB.RowTemplate.Height = 23;
            this.dgvShowDB.Size = new System.Drawing.Size(401, 458);
            this.dgvShowDB.TabIndex = 1;
            this.dgvShowDB.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvShowDB_CellClick);
            this.dgvShowDB.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvShowDB_CellFormatting);
            this.dgvShowDB.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvShowDB_RowPostPaint);
            // 
            // cmbTest
            // 
            this.cmbTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTest.FormattingEnabled = true;
            this.cmbTest.Location = new System.Drawing.Point(0, 0);
            this.cmbTest.Name = "cmbTest";
            this.cmbTest.Size = new System.Drawing.Size(179, 24);
            this.cmbTest.TabIndex = 3;
            this.cmbTest.SelectedIndexChanged += new System.EventHandler(this.cmbTest_SelectedIndexChanged);
            // 
            // myChart
            // 
            this.myChart.BorderlineColor = System.Drawing.Color.Gray;
            this.myChart.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea1.AxisX.Title = "Displacement(mm)";
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea1.AxisY.Title = "Compression<-Force(N)-> Rebound";
            chartArea1.Name = "ChartArea1";
            this.myChart.ChartAreas.Add(chartArea1);
            legend1.Alignment = System.Drawing.StringAlignment.Far;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend1.Name = "Legend1";
            this.myChart.Legends.Add(legend1);
            this.myChart.Location = new System.Drawing.Point(0, 52);
            this.myChart.Name = "myChart";
            this.myChart.Size = new System.Drawing.Size(454, 405);
            this.myChart.TabIndex = 4;
            this.myChart.Text = "chart1";
            this.myChart.GetToolTipText += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs>(this.myChart_GetToolTipText);
            // 
            // myChart2
            // 
            this.myChart2.BorderlineColor = System.Drawing.Color.Gray;
            this.myChart2.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea2.AxisX.Title = "Velocity(m/s)";
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea2.AxisY.Title = "Compression<-Force(N)-> Rebound";
            chartArea2.Name = "ChartArea1";
            this.myChart2.ChartAreas.Add(chartArea2);
            legend2.Alignment = System.Drawing.StringAlignment.Far;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend2.Name = "Legend1";
            this.myChart2.Legends.Add(legend2);
            this.myChart2.Location = new System.Drawing.Point(466, 51);
            this.myChart2.Name = "myChart2";
            this.myChart2.Size = new System.Drawing.Size(454, 405);
            this.myChart2.TabIndex = 5;
            this.myChart2.Text = "chart2";
            this.myChart2.GetToolTipText += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs>(this.myChart2_GetToolTipText);
            // 
            // pnlContain
            // 
            this.pnlContain.Controls.Add(this.myChart);
            this.pnlContain.Controls.Add(this.myChart2);
            this.pnlContain.Controls.Add(this.cmbTest);
            this.pnlContain.Location = new System.Drawing.Point(503, 186);
            this.pnlContain.Name = "pnlContain";
            this.pnlContain.Size = new System.Drawing.Size(922, 458);
            this.pnlContain.TabIndex = 6;
            this.pnlContain.Visible = false;
            // 
            // dtpStart
            // 
            this.dtpStart.CustomFormat = "   ";
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(503, 81);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(200, 23);
            this.dtpStart.TabIndex = 7;
            this.dtpStart.ValueChanged += new System.EventHandler(this.dtpStart_ValueChanged);
            // 
            // dtpEnd
            // 
            this.dtpEnd.CustomFormat = "   ";
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(757, 81);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(200, 23);
            this.dtpEnd.TabIndex = 8;
            this.dtpEnd.ValueChanged += new System.EventHandler(this.dtpEnd_ValueChanged);
            // 
            // lblSelectDate
            // 
            this.lblSelectDate.AutoSize = true;
            this.lblSelectDate.Location = new System.Drawing.Point(393, 84);
            this.lblSelectDate.Name = "lblSelectDate";
            this.lblSelectDate.Size = new System.Drawing.Size(106, 17);
            this.lblSelectDate.TabIndex = 9;
            this.lblSelectDate.Text = "请选择时间段：";
            // 
            // lblSelectTo
            // 
            this.lblSelectTo.AutoSize = true;
            this.lblSelectTo.Location = new System.Drawing.Point(724, 86);
            this.lblSelectTo.Name = "lblSelectTo";
            this.lblSelectTo.Size = new System.Drawing.Size(22, 17);
            this.lblSelectTo.TabIndex = 10;
            this.lblSelectTo.Text = "至";
            // 
            // btnSearchByTime
            // 
            this.btnSearchByTime.Location = new System.Drawing.Point(997, 78);
            this.btnSearchByTime.Name = "btnSearchByTime";
            this.btnSearchByTime.Size = new System.Drawing.Size(108, 27);
            this.btnSearchByTime.TabIndex = 11;
            this.btnSearchByTime.Text = "SearchByDate";
            this.btnSearchByTime.UseVisualStyleBackColor = true;
            this.btnSearchByTime.Click += new System.EventHandler(this.btnSearchByDate_Click);
            // 
            // lblSelectByNo
            // 
            this.lblSelectByNo.AutoSize = true;
            this.lblSelectByNo.Location = new System.Drawing.Point(363, 120);
            this.lblSelectByNo.Name = "lblSelectByNo";
            this.lblSelectByNo.Size = new System.Drawing.Size(136, 17);
            this.lblSelectByNo.TabIndex = 12;
            this.lblSelectByNo.Text = "请输入PartNo名称：";
            // 
            // txtSelectInput
            // 
            this.txtSelectInput.Location = new System.Drawing.Point(503, 117);
            this.txtSelectInput.Name = "txtSelectInput";
            this.txtSelectInput.Size = new System.Drawing.Size(200, 23);
            this.txtSelectInput.TabIndex = 13;
            // 
            // btnSearchByNo
            // 
            this.btnSearchByNo.Location = new System.Drawing.Point(997, 111);
            this.btnSearchByNo.Name = "btnSearchByNo";
            this.btnSearchByNo.Size = new System.Drawing.Size(108, 26);
            this.btnSearchByNo.TabIndex = 14;
            this.btnSearchByNo.Text = "SearchByNo";
            this.btnSearchByNo.UseVisualStyleBackColor = true;
            this.btnSearchByNo.Click += new System.EventHandler(this.btnSearchByNo_Click);
            // 
            // btnSearchAll
            // 
            this.btnSearchAll.Location = new System.Drawing.Point(1147, 110);
            this.btnSearchAll.Name = "btnSearchAll";
            this.btnSearchAll.Size = new System.Drawing.Size(75, 27);
            this.btnSearchAll.TabIndex = 15;
            this.btnSearchAll.Text = "Search";
            this.btnSearchAll.UseVisualStyleBackColor = true;
            this.btnSearchAll.Click += new System.EventHandler(this.btnSearchAll_Click);
            // 
            // btnClearDate
            // 
            this.btnClearDate.Location = new System.Drawing.Point(1147, 78);
            this.btnClearDate.Name = "btnClearDate";
            this.btnClearDate.Size = new System.Drawing.Size(75, 26);
            this.btnClearDate.TabIndex = 16;
            this.btnClearDate.Text = "Clear";
            this.btnClearDate.UseVisualStyleBackColor = true;
            this.btnClearDate.Click += new System.EventHandler(this.btnClearDate_Click);
            // 
            // pnlbg
            // 
            this.pnlbg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.pnlbg.Controls.Add(this.lblSelectDate);
            this.pnlbg.Controls.Add(this.btnClearDate);
            this.pnlbg.Controls.Add(this.dgvShowDB);
            this.pnlbg.Controls.Add(this.btnSearchAll);
            this.pnlbg.Controls.Add(this.pnlContain);
            this.pnlbg.Controls.Add(this.btnSearchByNo);
            this.pnlbg.Controls.Add(this.dtpStart);
            this.pnlbg.Controls.Add(this.txtSelectInput);
            this.pnlbg.Controls.Add(this.dtpEnd);
            this.pnlbg.Controls.Add(this.lblSelectByNo);
            this.pnlbg.Controls.Add(this.lblSelectTo);
            this.pnlbg.Controls.Add(this.btnSearchByTime);
            this.pnlbg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlbg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.pnlbg.Location = new System.Drawing.Point(0, 0);
            this.pnlbg.Name = "pnlbg";
            this.pnlbg.Size = new System.Drawing.Size(1518, 750);
            this.pnlbg.TabIndex = 17;
            // 
            // TestResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1518, 750);
            this.Controls.Add(this.pnlbg);
            this.Name = "TestResultForm";
            this.Text = "TestResultForm";
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myChart2)).EndInit();
            this.pnlContain.ResumeLayout(false);
            this.pnlbg.ResumeLayout(false);
            this.pnlbg.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DataGridView dgvShowDB;
        private ComboBox cmbTest;
        private Chart myChart;
        private Chart myChart2;
        private Panel pnlContain;
        private DateTimePicker dtpStart;
        private DateTimePicker dtpEnd;
        private Label lblSelectDate;
        private Label lblSelectTo;
        private Button btnSearchByTime;
        private Label lblSelectByNo;
        private TextBox txtSelectInput;
        private Button btnSearchByNo;
        private Button btnSearchAll;
        private Button btnClearDate;
        private Panel pnlbg;
    }
}