﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DampereConnectDBProj
{
    public class TestResult
    {
        public string PartNo { get; set; }
        public int SampleRate { get; set; }
        public DateTime AddDate { get; set; }
        public List<SingleTestResult> Tests { get; set; }
    }
    public class SingleTestResult
    {
        public bool Pass { get; set; }
        public List<SpeedResult> SpeedResults { get; set; }

        public string TestNo { get; set; }
        public string PartNo { get; set; }
    }
    public class SpeedResult
    {
        public float ReboundForce { get; set; }
        public float CompressionForce { get; set; }
        public List<float> Displacement { get; set; }
        public List<float> Velocity { get; set; }
        public List<float> Force { get; set; }
        public List<float> CompressionMark { get; set; }
        public List<float> ReboundMark { get; set; }

        public string SpeedNo { get; set; }
        public string TestNo { get; set; }

    }
}
