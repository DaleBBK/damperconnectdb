﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DampereConnectDBProj
{
    public partial class TestDLLForm : Form
    {
        public TestDLLForm()
        {
            InitializeComponent();
            TestInsertData();
        }

        public void TestInsertData()
        {
            TestResultForm form = new TestResultForm();

            TestResult result = new TestResult();
            result.PartNo = "PN20170727001";
            result.SampleRate = new Random().Next(10);
            result.AddDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            form.InsertIntoDamperConfig(result);



            int countTest = 2;
            SingleTestResult singleRs = null;
            List<SingleTestResult> testLst = new List<SingleTestResult>();
            for (int i = 1; i <= countTest; i++)
            {
                singleRs = new SingleTestResult();
                //PartNo关联关系
                singleRs.PartNo = result.PartNo;

                if (i.ToString().Length == 1)
                {
                    singleRs.TestNo = "TN" + result.PartNo.Substring(2, 11) + "00" + i;
                }
                if (i.ToString().Length == 2)
                {
                    singleRs.TestNo = "TN" + result.PartNo.Substring(2, 11) + "0" + i;
                }
                if (i.ToString().Length == 3)
                {
                    singleRs.TestNo = "TN" + result.PartNo.Substring(2, 11) + i;
                }

                //前包后不包，随机数是0或1
                singleRs.Pass = new Random().Next(0, 2) == 1 ? true : false;

                testLst.Add(singleRs);
            }
            form.InsertIntoDapmerTest(result.PartNo, singleRs, testLst, countTest);



            int countSpeed = 2;
            string dispStrSplit = "", veloStrSplit = "", forceStrSplit = "", compStrSplit = "", reboStrSplit = "";
            SpeedResult speedRs = null;
            List<SpeedResult> speedLst = new List<SpeedResult>();
            List<string> testNoList = form.GetTestNoList(result.PartNo);

            //根据筛选出来的TestNo往第三张表添加数据
            string speedNo = "";
            for (int j = 0; j < testNoList.Count; j++)
            {

                for (int k = 1; k <= countSpeed; k++)
                {

                    speedRs = new SpeedResult();
                    //TestNo
                    speedRs.TestNo = testNoList[j];

                    //SpeedNo
                    if (k.ToString().Length == 1)
                    {
                        speedNo = "SN" + speedRs.TestNo.Substring(2, 14) + "00" + k;
                    }
                    if (k.ToString().Length == 2)
                    {
                        speedNo = "SN" + speedRs.TestNo.Substring(2, 14) + "0" + k;
                    }
                    if (k.ToString().Length == 3)
                    {
                        speedNo = "SN" + speedRs.TestNo.Substring(2, 14) + k;
                    }
                    speedRs.SpeedNo = speedNo;

                    //ReboundForce
                    List<float> rltList = form.GetSingleFloatVal();
                    speedRs.ReboundForce = rltList[0];

                    //CompressionForce
                    speedRs.CompressionForce = rltList[1];

                    //Displacement
                    float[] fArray = form.getFloatArray();
                    speedRs.Displacement = form.getList(fArray);

                    //Velocity
                    fArray = form.getFloatArray();
                    speedRs.Velocity = form.getList(fArray);

                    //Force
                    fArray = form.getFloatArray();
                    speedRs.Force = form.getList(fArray);

                    //ReboundMark
                    fArray = form.getFloatArray();
                    speedRs.ReboundMark = form.getList(fArray);

                    //CompressionMark
                    fArray = form.getFloatArray();
                    speedRs.CompressionMark = form.getList(fArray);
                    

                    speedLst.Add(speedRs);

                    #region 五组数
                    for (int q = 0; q < speedLst.Count; q++)
                    {
                        List<float> fflst = speedLst[q].Displacement;
                        string dispStr = "";
                        for (int i = 0; i < fflst.Count; i++)
                        {
                            dispStr += fflst[i] + ",";
                        }
                        dispStrSplit = dispStr.Substring(0, dispStr.Length - 1);


                        fflst = speedLst[q].Velocity;
                        string veloStr = "";
                        for (int i = 0; i < fflst.Count; i++)
                        {
                            veloStr += fflst[i] + ",";
                        }
                        veloStrSplit = veloStr.Substring(0, veloStr.Length - 1);


                        fflst = speedLst[q].Force;
                        string forceStr = "";
                        for (int i = 0; i < fflst.Count; i++)
                        {
                            forceStr += fflst[i] + ",";
                        }
                        forceStrSplit = forceStr.Substring(0, forceStr.Length - 1);


                        fflst = speedLst[q].CompressionMark;
                        string compStr = "";
                        for (int i = 0; i < fflst.Count; i++)
                        {
                            compStr += fflst[i] + ",";
                        }
                        compStrSplit = compStr.Substring(0, compStr.Length - 1);


                        fflst = speedLst[q].ReboundMark;
                        string reboStr = "";
                        for (int i = 0; i < fflst.Count; i++)
                        {
                            reboStr += fflst[i] + ",";
                        }
                        reboStrSplit = reboStr.Substring(0, reboStr.Length - 1);

                    }
                    #endregion

                    form.InsertIntoDamperSpeed(speedRs.SpeedNo, speedRs.TestNo, speedRs.ReboundForce, speedRs.CompressionForce, dispStrSplit, veloStrSplit, forceStrSplit, compStrSplit, reboStrSplit);

                }


            }

           
        }
    }
}
