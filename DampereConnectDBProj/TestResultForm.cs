﻿using DampereConnectDBProj;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace DampereConnectDBProj
{
    public partial class TestResultForm : Form
    {
        private SqlCommand objSqlCommand;
        private SqlConnection objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["conStr"].ConnectionString);
        private string sqlConnection = ConfigurationManager.AppSettings["ConnectionString"];
        private string selectedValue = "";
        private string testNo = "";
        Random rdm = new Random();
        Random ranInt = new Random();
        Random ranFloat = new Random();

        public TestResultForm()
        {
            InitializeComponent();


            ShowBaseData();


            for (int i = 0; i < this.dgvShowDB.Columns.Count; i++)
            {
                this.dgvShowDB.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;//设置单元格内容居中后标题再次居中，否则标题整体偏左
                this.dgvShowDB.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;//设置各个列宽自动填充满DGV
                //窗口初始化时绑定数据会报错“为自动填充列调整大小期间不能执行此操作”，添加下面设置不会报错
                this.dgvShowDB.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            }


            //添加数据至 BBK_DamperConfig
            //InsertIntoDamperConfig();
            //添加数据至 BBK_DamperTest（传入参数，Test个数）
            //InsertIntoDapmerTest(1);
            //添加数据至 BBK_DamperSpeed（传入参数，Speed个数）
            //InsertIntoDamperSpeed(3);

        }

        #region ==== 初始化数据显示、DGV设置

        DataTable dtBase = null;
        //显示BBK_DamperConfig基本数据
        public void ShowBaseData()
        {
            string sql = @"select dc.PartNo,dc.AddDate,case when dc.Pass='1' then 'Yes' else 'No' end Pass from [BBK_Config].[dbo].[BBK_DamperConfig] dc";
            dtBase = QueryDT(sql, sqlConnection);
            this.dgvShowDB.DataSource = dtBase;
        }

        //设置DataGridView行号
        private void dgvShowDB_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = (DataGridView)sender;
            if (dgv.RowHeadersVisible)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, dgv.RowHeadersWidth, e.RowBounds.Height);
                rect.Inflate(-2, -2);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, rect, e.InheritedRowStyle.ForeColor,
                    TextFormatFlags.Right | TextFormatFlags.VerticalCenter);

            }
        }
        //绑定某列进行颜色设置
        private void dgvShowDB_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex < 0) { return; }

            DataGridViewRow dgr = this.dgvShowDB.Rows[e.RowIndex];
            try
            {
                if (e.ColumnIndex == 2)//定位列数变化颜色
                {

                    if (dgr.Cells[2].Value.ToString() == "Yes")
                    {
                        e.CellStyle.ForeColor = Color.Green;
                    }
                    else if (dgr.Cells[2].Value.ToString() == "No")
                    {
                        e.CellStyle.ForeColor = Color.Red;
                    }
                    else if (dgr.Cells[2].Value.ToString() == "null")
                    {
                        e.CellStyle.ForeColor = Color.Gray;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        //绑定右侧列表图
        private void dgvShowDB_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //获取单击的单元格的值
                selectedValue = this.dgvShowDB.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                if (selectedValue.StartsWith("PN2017") && !selectedValue.Equals("PartNo"))
                {
                    //点击出现图（按照控件由近及远顺序）
                    this.pnlContain.Visible = true;
                    this.myChart.Visible = true;


                    //点击出现右侧级联内容
                    this.ShowTestNoList(selectedValue);
                }
            }
            catch
            {
                MessageBox.Show("Please click on the correct cell!");
            }

        }

        //读取PartNo对应所有TestNo
        private void ShowTestNoList(string partNo)
        {
            objSqlConnection.Open();
            string sql = @"select dt.ID,dt.TestNo,dt.PartNo from [BBK_Config].[dbo].[BBK_DamperTest] dt where dt.PartNo='" + partNo + "'";
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(sql, objSqlConnection);
            //方法一DataSet
            DataSet ds = new DataSet();
            objSqlDataAdapter.Fill(ds, "Test");
            this.cmbTest.DataSource = ds.Tables["Test"];
            //方法二DataTable
            //DataTable dt = new DataTable();
            //objSqlDataAdapter.Fill(dt);
            //this.cmbTest.DataSource = dt;
            this.cmbTest.DisplayMember = "TestNo";
            this.cmbTest.ValueMember = "PartNo";
            objSqlConnection.Close();
        }

        #endregion


        #region ==== 添加数据至数据库表

        //读取PartNo方法
        public DataTable GetLastPartNo()
        {
            DataTable dt = new DataTable();
            objSqlConnection.Open();
            string sqlLastNo = @"select top 1 dc.PartNo from [BBK_Config].[dbo].[BBK_DamperConfig] dc order by dc.ID desc";
            objSqlCommand = new SqlCommand(sqlLastNo, objSqlConnection);
            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
            objSqlDataAdapter.Fill(dt);
            objSqlConnection.Close();
            return dt;
        }


        //添加数据至 BBK_DamperConfig
        public void InsertIntoDamperConfig(TestResult testRs)
        {
            //DataTable dt = this.GetLastPartNo();//调用读取PartNo方法

            //string partNo = "";
            //string subPartNo = "";
            //if (dt.Rows.Count > 0)
            //{
            //    partNo = dt.Rows[0]["PartNo"].ToString();//获取数据库表最后一条记录的PartNo数据
            //}


            ////判断上一条No的时间与当前时间是否相符(从第2位数共8位)，相符进行位数判断，不相符则从当前时间+001开始计数
            //if (partNo.Substring(2, 8) == DateTime.Now.ToString("yyyyMMdd"))
            //{
            //    subPartNo = (int.Parse(partNo.Substring(partNo.Length - 3)) + 1).ToString();

            //    //判断截取的后三位+1后长度，匹配相应前缀0
            //    if (subPartNo.Length == 1)
            //    {
            //        testRs.PartNo = "PN" + DateTime.Now.ToString("yyyyMMdd") + "00" + subPartNo;
            //    }
            //    else if (subPartNo.Length == 2)
            //    {
            //        testRs.PartNo = "PN" + DateTime.Now.ToString("yyyyMMdd") + "0" + subPartNo;
            //    }
            //    else if (subPartNo.Length == 3)
            //    {
            //        testRs.PartNo = "PN" + DateTime.Now.ToString("yyyyMMdd") + subPartNo;
            //    }
            //}
            //else
            //{
            //    testRs.PartNo = "PN" + DateTime.Now.ToString("yyyyMMdd") + "001";
            //}

            //testRs.SampleRate = new Random().Next(10);
            //testRs.AddDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            //执行插入数据方法，int返回值做测试用
            this.ExecuteSql(@"insert into [BBK_Config].[dbo].[BBK_DamperConfig](PartNo,SampleRate,AddDate) values('" + testRs.PartNo + "'," + testRs.SampleRate + ",'" + testRs.AddDate + "')");
        }


        //添加数据至 BBK_DamperTest
        public void InsertIntoDapmerTest(string partNo, SingleTestResult singleRs, List<SingleTestResult> testLst, int countTest)
        {
            //DataTable dt = this.GetLastPartNo();//调用读取PartNo方法

            //string testNo = "";
            //if (dt.Rows.Count > 0)
            //{
            //    partNo = dt.Rows[0]["PartNo"].ToString();//获取数据库表最后一条记录的PartNo数据
            //}

            //List<SingleTestResult> testLst = new List<SingleTestResult>();

            //for (int i = 1; i <= countTest; i++)
            //{
            //    singleRs = new SingleTestResult();
            //    //PartNo关联关系
            //    singleRs.PartNo = partNo;

            //    if (i.ToString().Length == 1)
            //    {
            //        testNo = "TN" + partNo.Substring(2, 11) + "00" + i;
            //    }
            //    if (i.ToString().Length == 2)
            //    {
            //        testNo = "TN" + partNo.Substring(2, 11) + "0" + i;
            //    }
            //    if (i.ToString().Length == 3)
            //    {
            //        testNo = "TN" + partNo.Substring(2, 11) + i;
            //    }
            //    singleRs.TestNo = testNo;

            //    //前包后不包，随机数是0或1
            //    singleRs.Pass = new Random().Next(0, 2) == 1 ? true : false;

            //    testLst.Add(singleRs);
            //}

            //循环插入数据库    
            for (int j = 0; j < testLst.Count; j++)
            {
                //执行插入语句
                this.ExecuteSql(@"insert into [BBK_Config].[dbo].[BBK_DamperTest](TestNo,PartNo,IsPass) values('" + testLst[j].TestNo + "','" + testLst[j].PartNo + "'," + (testLst[j].Pass == true ? 1 : 0) + ")");
            }

            //根据每个TestNo是否合格结果判断最终的所属PartNo是否合格
            DataTable dtPass = this.QueryDT(@"select dt.IsPass from [BBK_Config].[dbo].[BBK_DamperTest] dt where dt.PartNo='" + partNo + "'", sqlConnection);
            for (int b = 0; b < dtPass.Rows.Count; b++)
            {
                if (dtPass.Rows[b][0].ToString() == "True")
                {
                    this.ExecuteSql(@"update [BBK_Config].[dbo].[BBK_DamperConfig] set Pass=1 where PartNo='" + partNo + "'");
                }
                else
                {
                    this.ExecuteSql(@"update [BBK_Config].[dbo].[BBK_DamperConfig] set Pass=0 where PartNo='" + partNo + "'");
                }
            }

        }

        public List<string> GetTestNoList(string partNo)
        {
            string sqlLastNo = @"select dt.TestNo from [BBK_Config].[dbo].[BBK_DamperTest] dt where dt.PartNo='"+ partNo + "'";
            DataTable dt = this.QueryDT(sqlLastNo, sqlConnection);

            List<string> testNoList = new List<string>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    testNo = dt.Rows[i]["TestNo"].ToString();
                    if (testNo.Substring(2, 8) == DateTime.Now.ToString("yyyyMMdd"))
                    {
                        testNoList.Add(testNo);
                    }
                }
            }
            return testNoList;
        }

        //添加数据至 BBK_DamperSpeed
        public void InsertIntoDamperSpeed(string speedNo,string testNo, float ReboundForce, float CompressionForce,
            string dispStrSplit,string veloStrSplit, string forceStrSplit, string compStrSplit, string reboStrSplit)
        {

            #region
            //speedLst = new List<SpeedResult>();

            //string sqlLastNo = @"select dt.TestNo from [BBK_Config].[dbo].[BBK_DamperTest] dt";
            //DataTable dt = this.QueryDT(sqlLastNo, sqlConnection);


            //testNoList = new List<string>();
            //if (dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        testNo = dt.Rows[i]["TestNo"].ToString();
            //        if (testNo.Substring(2, 8) == DateTime.Now.ToString("yyyyMMdd"))
            //        {
            //            testNoList.Add(testNo);
            //        }
            //    }
            //}

            //根据筛选出来的TestNo往第三张表添加数据
            //string speedNo = "";
            //for (int j = 0; j < testNoList.Count; j++)
            //{

            //    for (int k = 1; k <= countSpeed; k++)
            //    {

            //        speedRs = new SpeedResult();
            //        //TestNo
            //        speedRs.TestNo = testNoList[j];

            //        //SpeedNo
            //        if (k.ToString().Length == 1)
            //        {
            //            speedNo = "SN" + speedRs.TestNo.Substring(2, 14) + "00" + k;
            //        }
            //        if (k.ToString().Length == 2)
            //        {
            //            speedNo = "SN" + speedRs.TestNo.Substring(2, 14) + "0" + k;
            //        }
            //        if (k.ToString().Length == 3)
            //        {
            //            speedNo = "SN" + speedRs.TestNo.Substring(2, 14) + k;
            //        }
            //        speedRs.SpeedNo = speedNo;

            //        //ReboundForce
            //        List<float> rltList = this.GetSingleFloatVal();
            //        speedRs.ReboundForce = rltList[0];

            //        //CompressionForce
            //        speedRs.CompressionForce = rltList[1];

            //        //Displacement
            //        float[] fArray = this.getFloatArray();
            //        speedRs.Displacement = this.getList(fArray);

            //        //Velocity
            //        fArray = this.getFloatArray();
            //        speedRs.Velocity = this.getList(fArray);

            //        //Force
            //        fArray = this.getFloatArray();
            //        speedRs.Force = this.getList(fArray);

            //        //ReboundMark
            //        fArray = this.getFloatArray();
            //        speedRs.ReboundMark = this.getList(fArray);

            //        //CompressionMark
            //        fArray = this.getFloatArray();
            //        speedRs.CompressionMark = this.getList(fArray);

            //    }
            //    speedLst.Add(speedRs);
            //}

            //for (int q = 0; q < speedLst.Count; q++)
            //{
                //    List<float> fflst = speedLst[q].Displacement;
                //    string dispStr = "";
                //    for (int i = 0; i < fflst.Count; i++)
                //    {
                //        dispStr += fflst[i] + ",";
                //    }
                //    string dispStrSplit = dispStr.Substring(0, dispStr.Length - 1);


                //    fflst = speedLst[q].Velocity;
                //    string veloStr = "";
                //    for (int i = 0; i < fflst.Count; i++)
                //    {
                //        veloStr += fflst[i] + ",";
                //    }
                //    string veloStrSplit = veloStr.Substring(0, veloStr.Length - 1);


                //    fflst = speedLst[q].Force;
                //    string forceStr = "";
                //    for (int i = 0; i < fflst.Count; i++)
                //    {
                //        forceStr += fflst[i] + ",";
                //    }
                //    string forceStrSplit = forceStr.Substring(0, forceStr.Length - 1);


                //    fflst = speedLst[q].CompressionMark;
                //    string compStr = "";
                //    for (int i = 0; i < fflst.Count; i++)
                //    {
                //        compStr += fflst[i] + ",";
                //    }
                //    string compStrSplit = compStr.Substring(0, compStr.Length - 1);


                //    fflst = speedLst[q].ReboundMark;
                //    string reboStr = "";
                //    for (int i = 0; i < fflst.Count; i++)
                //    {
                //        reboStr += fflst[i] + ",";
                //    }
                //    string reboStrSplit = reboStr.Substring(0, reboStr.Length - 1);
               

            //    this.ExecuteSql(@"insert into [BBK_Config].[dbo].[BBK_DamperSpeed](SpeedNo,TestNo,ReboundForce,CompressionForce,Displacement,Velocity,[Force],CompressionMark,ReboundMark)
            //                    values('" + speedLst[q].SpeedNo + "','" + speedLst[q].TestNo + "'," + speedLst[q].ReboundForce + "," + speedLst[q].CompressionForce + ",'"
            //                    + dispStrSplit + "','" + veloStrSplit + "','" + forceStrSplit + "','" + compStrSplit + "','" + reboStrSplit + "')");
            //}
            #endregion


            this.ExecuteSql(@"insert into [BBK_Config].[dbo].[BBK_DamperSpeed](SpeedNo,TestNo,ReboundForce,CompressionForce,Displacement,Velocity,[Force],CompressionMark,ReboundMark)
                                values('" + speedNo + "','" + testNo + "'," + ReboundForce + "," + CompressionForce + ",'"
                                + dispStrSplit + "','" + veloStrSplit + "','" + forceStrSplit + "','" + compStrSplit + "','" + reboStrSplit + "')");
        }

        #endregion


        #region ==== 随机数、封装的数据库方法
        public List<float> GetSingleFloatVal()
        {
            List<float> result = new List<float>();
            int ints;
            double floats;
            float val;
            for (int i = 0; i < 2; i++)
            {
                ints = ranInt.Next(0, 100);
                floats = ranFloat.NextDouble();
                val = float.Parse((ints + floats).ToString("F2"));
                if (i == 0)
                {
                    result.Add(val);
                }
                else
                {
                    if (result.IndexOf(val) > 0)
                    {
                        i = i - 1; //有重复值,重新再来一次
                    }
                    else
                    {
                        result.Add(val);  //无重复值，+1
                    }
                }
            }
            return result;
        }

        public List<string> GetArrayFloatVal()
        {
            List<string> newList = new List<string>();

            string text;

            #region ==== 随机生成五组数据
            for (int i = 0; i < 10; i++)
            {
                #region ==== 随机生成十个数为一组数据
                List<float> result = new List<float>();
                float a;
                for (int j = 0; j < 10; j++)
                {

                    double d = Math.Round(rdm.NextDouble() * (1 - 0) + 0, 2);
                    a = (float)Convert.ToDouble(d);
                    if (j == 0)
                    {
                        result.Add(a);
                    }
                    else
                    {
                        if (result.IndexOf(a) > 0)
                        {
                            j = j - 1; //有重复值,重新再来一次
                        }
                        else
                        {
                            result.Add(a);  //无重复值，+1
                        }
                    }
                }
                #endregion

                string items = "";
                for (int m = 0; m < result.Count; m++)
                {
                    items += result[m] + ",";
                }

                text = items.Substring(0, items.Length - 1);
                newList.Add(text);
            }
            #endregion

            return newList;
        }

        //只获取一组随机数
        public float[] getFloatArray()
        {
            float[] fArray = new float[10];

            for (int i = 0; i < fArray.Length; i++)
            {
                double d = Math.Round(rdm.NextDouble() * (1 - 0) + 0, 2);
                fArray[i] = (float)Convert.ToDouble(d);
            }
            return fArray;
        }


        //数组类型转List<float>类型
        public List<float> getList(float[] obj)
        {
            List<float> objLst = new List<float>();
            for (int i = 0; i < obj.Length; i++)
            {
                objLst.Add(obj[i]);
            }
            return objLst;
        }


        //执行添加数据库数据方法
        public int ExecuteSql(string SQLString)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnection))
            {
                using (SqlCommand cmd = new SqlCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        int rows = cmd.ExecuteNonQuery();
                        return rows;
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        connection.Close();
                        throw e;
                    }
                }
            }
        }

        //返回DataTable方法
        public DataTable QueryDT(string strSql, string strConn)
        {
            objSqlConnection.Close();
            objSqlConnection.Open();
            DataSet ds = new DataSet();
            objSqlConnection = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter(strSql, objSqlConnection);
            da.Fill(ds, "Table1");
            objSqlConnection.Close();
            return ds.Tables[0];
        }

        //返回DataSet方法
        public DataSet QueryDS(string SQLString)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnection))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlDataAdapter command = new SqlDataAdapter(SQLString, connection);
                    command.Fill(ds, "ds");
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds;
            }
        }

        #endregion


        #region ==== chart作图

        public void FrmMain_Load(string cmbTestNo)
        {
            string sql = @"select count(ds.ID) cnt from [BBK_Config].[dbo].[BBK_DamperSpeed] ds where ds.TestNo='" + cmbTestNo + "'";
            DataTable dtCnt = this.QueryDT(sql, sqlConnection);

            int cnt = 0;
            if (dtCnt.Rows.Count > 0)
            {
                cnt = int.Parse(dtCnt.Rows[0]["cnt"].ToString());
            }

            float[][] xDataArray = new float[cnt][];
            float[][] yDataArray = new float[cnt][];
            float[][] xDataArray2 = new float[cnt][];

            string xSql = @"select ds.Displacement from [BBK_Config].[dbo].[BBK_DamperSpeed] ds where ds.TestNo='" + cmbTestNo + "'";
            DataTable dtCntX = this.QueryDT(xSql, sqlConnection);

            string ySql = @"select ds.Force from [BBK_Config].[dbo].[BBK_DamperSpeed] ds where ds.TestNo='" + cmbTestNo + "'";
            DataTable dtCntY = this.QueryDT(ySql, sqlConnection);

            string xSql2 = @"select ds.Velocity from [BBK_Config].[dbo].[BBK_DamperSpeed] ds where ds.TestNo='" + cmbTestNo + "'";
            DataTable dtCntX2 = this.QueryDT(xSql2, sqlConnection);

            if (dtCntX.Rows.Count > 0)
            {
                string sub = "";
                string[] strs = null;
                int len = 0;
                for (int k = 0; k < dtCntX.Rows.Count; k++)
                {
                    sub = dtCntX.Rows[k]["Displacement"].ToString();
                    strs = sub.Split(',');

                    len = strs.Length;

                    float[] fArr = new float[len];
                    for (int i = 0; i < len; i++)
                    {
                        fArr[i] = float.Parse(strs[i]);
                    }
                    xDataArray[k] = fArr;
                }

                for (int l = 0; l < dtCntY.Rows.Count; l++)
                {
                    sub = dtCntY.Rows[l]["Force"].ToString();
                    strs = sub.Split(',');

                    len = strs.Length;

                    float[] fArr = new float[len];
                    for (int j = 0; j < len; j++)
                    {
                        fArr[j] = float.Parse(strs[j]);
                    }
                    yDataArray[l] = fArr;
                }

                for (int a = 0; a < dtCntX2.Rows.Count; a++)
                {
                    sub = dtCntX2.Rows[a]["Velocity"].ToString();
                    strs = sub.Split(',');

                    len = strs.Length;

                    float[] fArr = new float[len];
                    for (int b = 0; b < len; b++)
                    {
                        fArr[b] = float.Parse(strs[b]);
                    }
                    xDataArray2[a] = fArr;
                }


                for (int i = 0; i < xDataArray.Length; i++)
                {

                    Series series = this.SetSeriesStyle(i);

                    for (int j = 0; j < xDataArray[i].Length; j++)
                    {
                        float xx = float.Parse(xDataArray[i][j].ToString());
                        series.Points.AddXY(Math.Round(xx, 2), yDataArray[i][j]);
                        //series.Points.AddXY(xDataArray[i][j], yDataArray[i][j]);
                    }
                    this.myChart.Series.Add(series);
                }


                //第二个图就要用不同的数组变量值
                for (int m = 0; m < xDataArray2.Length; m++)
                {
                    Series series = this.SetSeriesStyle(m);

                    for (int n = 0; n < xDataArray2[m].Length; n++)
                    {
                        float xx = float.Parse(xDataArray2[m][n].ToString());
                        series.Points.AddXY(Math.Round(xx, 2), yDataArray[m][n]);
                    }
                    this.myChart2.Series.Add(series);
                }

            }

        }

        //鼠标悬浮的提示框
        private void myChart_GetToolTipText(object sender, ToolTipEventArgs e)
        {
            if (e.HitTestResult.ChartElementType == ChartElementType.DataPoint)
            {
                int i = e.HitTestResult.PointIndex;
                DataPoint dp = e.HitTestResult.Series.Points[i];
                e.Text = string.Format("位移:{0}mm; 力:{1:F1}N", dp.XValue, dp.YValues[0]);
            }
        }
        //鼠标悬浮的提示框
        private void myChart2_GetToolTipText(object sender, ToolTipEventArgs e)
        {
            if (e.HitTestResult.ChartElementType == ChartElementType.DataPoint)
            {
                int i = e.HitTestResult.PointIndex;
                DataPoint dp = e.HitTestResult.Series.Points[i];
                e.Text = string.Format("速度:{0}m/s; 力:{1:F1}N", dp.XValue, dp.YValues[0]);
            }
        }

        //设置Series样式
        private Series SetSeriesStyle(int i)
        {
            Series series = new Series(string.Format("Speed{0}", i + 1));

            //Series的类型
            series.ChartType = SeriesChartType.Line;
            //Series的边框颜色
            series.BorderColor = Color.FromArgb(180, 26, 59, 105);
            //线条宽度
            series.BorderWidth = 1;
            //线条阴影颜色
            series.ShadowColor = Color.Black;
            //阴影宽度
            series.ShadowOffset = 0;
            //是否显示数据说明
            series.IsVisibleInLegend = true;
            //线条上数据点上是否有数据显示
            series.IsValueShownAsLabel = false;
            //线条上的数据点标志类型
            series.MarkerStyle = MarkerStyle.Circle;
            //线条数据点的大小
            series.MarkerSize = 0;
            //线条颜色
            series.Palette = ChartColorPalette.BrightPastel;

            return series;
        }

        //根据下拉框值变化重新绘图
        private void cmbTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.myChart.Series.Clear();
            this.myChart2.Series.Clear();
            testNo = this.cmbTest.Text;
            this.FrmMain_Load(testNo);
        }

        #endregion


        #region ==== 设置日期控件初始化为空
        private void dtpStart_ValueChanged(object sender, EventArgs e)
        {
            this.dtpStart.CustomFormat = null;
        }

        private void dtpEnd_ValueChanged(object sender, EventArgs e)
        {
            this.dtpEnd.CustomFormat = null;
        }

        #endregion


        #region ==== 事件操作方法

        //时间段选择查询
        private void btnSearchByDate_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dtpStart.Text != "" && !string.IsNullOrEmpty(this.dtpStart.Text) && this.dtpEnd.Text != "" && !string.IsNullOrEmpty(this.dtpEnd.Text))
                {
                    DateTime startTime = Convert.ToDateTime(this.dtpStart.Text);
                    DateTime endTime = Convert.ToDateTime(this.dtpEnd.Text).AddDays(1);

                    if (DateTime.Compare(startTime, endTime) < 0)
                    {
                        string sql = @"select dc.PartNo,dc.AddDate,case when dc.Pass='1' then 'Yes' else 'No' end Pass from [BBK_Config].[dbo].[BBK_DamperConfig] dc where AddDate between '" + startTime + "' and '" + endTime + "';";
                        DataTable dt = this.QueryDT(sql, sqlConnection);
                        if (dt.Rows.Count > 0)
                        {
                            this.dgvShowDB.DataSource = dt;
                        }
                        else
                        {
                            MessageBox.Show("There is no data for this time. Please choose again!");
                            this.dgvShowDB.DataSource = dtBase;
                        }
                    }
                    else if (DateTime.Compare(startTime, endTime) == 0)
                    {
                        MessageBox.Show("The time you have selected is the same. Please choose again!");
                    }
                    else if (DateTime.Compare(startTime, endTime) > 0)
                    {
                        MessageBox.Show("The start time is later than the end time. Please choose again!");
                    }
                }
                else
                {
                    MessageBox.Show("Time is not allowed to empty. Please choose again!");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Time is not allowed to empty. Please choose again!");
            }

        }

        //清空按钮
        private void btnClearDate_Click(object sender, EventArgs e)
        {
            this.dtpStart.Format = DateTimePickerFormat.Custom;
            this.dtpStart.CustomFormat = " ";
            this.dtpEnd.Format = DateTimePickerFormat.Custom;
            this.dtpEnd.CustomFormat = " ";
            this.txtSelectInput.Text = " ";
            this.dgvShowDB.DataSource = dtBase;
        }

        //PartNo输入查询
        private void btnSearchByNo_Click(object sender, EventArgs e)
        {
            string inputNo = this.txtSelectInput.Text;
            if (inputNo == "" || string.IsNullOrEmpty(inputNo))
            {
                MessageBox.Show("The textbox must not be null!");
            }
            //else if (JudgeInput(inputNo) == false)
            //{
            //    MessageBox.Show("The value you entered is incorrect. Please re-enter it!");
            //}
            else
            {
                string sql = @"select dc.PartNo,dc.AddDate,case when dc.Pass='1' then 'Yes' else 'No' end Pass from [BBK_Config].[dbo].[BBK_DamperConfig] dc where dc.PartNo like '%" + inputNo + "%'";
                DataTable dt = this.QueryDT(sql, sqlConnection);
                this.dgvShowDB.DataSource = dt;
            }
        }
        //判断输入的值是否和数据库存储的值相同
        private bool JudgeInput(string value)
        {
            string sql = @"select dc.PartNo from [BBK_Config].[dbo].[BBK_DamperConfig] dc";
            DataTable dt = this.QueryDT(sql, sqlConnection);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (value == dt.Rows[i][0].ToString())
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        //时间段+编号同时查询
        private void btnSearchAll_Click(object sender, EventArgs e)
        {
            DateTime startTime = Convert.ToDateTime(this.dtpStart.Text);
            DateTime endTime = Convert.ToDateTime(this.dtpEnd.Text).AddDays(1);
            string inputNo = this.txtSelectInput.Text;

            if (this.dtpStart.Text == "" || string.IsNullOrEmpty(this.dtpStart.Text) || this.dtpEnd.Text == "" || string.IsNullOrEmpty(this.dtpEnd.Text))
            {
                MessageBox.Show("The textbox must not be null!");
            }
            if (DateTime.Compare(startTime, endTime) > 0 || DateTime.Compare(startTime, endTime) == 0)
            {
                MessageBox.Show("The time you have selected is incorrect!");
            }
            //if (JudgeInput(inputNo) == false)
            //{
            //    MessageBox.Show("The value you entered is incorrect!");
            //}

            if ((DateTime.Compare(startTime, endTime) < 0) && (inputNo != "") && (!string.IsNullOrEmpty(inputNo))/* && (JudgeInput(inputNo)==true)*/)
            {
                string sql = @"select dc.PartNo,dc.AddDate,case when dc.Pass='1' then 'Yes' else 'No' end Pass from [BBK_Config].[dbo].[BBK_DamperConfig] dc where (AddDate between '" + startTime + "' and '" + endTime + "') and dc.PartNo like '%" + inputNo + "%';";
                DataTable dt = this.QueryDT(sql, sqlConnection);
                this.dgvShowDB.DataSource = dt;
            }

        }

        #endregion





    }
}
